import request from '@/utils/request'

export function register(data) {
  return request({
    url: '/User/Create',
    method: 'get',
    params: data
  })
}
export function registerAdmin(data) {
  return request({
    url: '/User/registerAdmin',
    method: 'get',
    params: data
  })
}