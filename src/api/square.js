import request from '@/utils/request'

export function getAllSquare(query) {
  return request({
    url: 'square/getAllSquare/' + query.page + '/' + query.limit,
    method: 'get'
  })
}

export function DelectSquareById(id) {
  return request({
    url: 'square/DelectSquareById/' + id,
    method: 'get'
  })
}

export function ChangeSquareById(id,conteent) {
  return request({
    url: 'square/ChangeSquareById/' + id + '/' + conteent,
    method: 'get'
  })
}

export function onTopSquareById(id,ontop) {
  return request({
    url: 'square/onTopSquareById/' + id + '/' + ontop,
    method: 'get'
  })
}

export function GetAllSquareUser() {
  return request({
    url: 'square/GetAllSquareUser',
    method: 'get'
  })
}
export function GetAllSquareUserPage(query) {
  return request({
    url: 'square/GetAllSquareUserPage/' + query.page + '/' + query.limit,
    method: 'get'
  })
}

export function GetAllSquareUserAdminPage(query) {
  return request({
    url: 'square/GetAllSquareUserAdminPage/' + query.page + '/' + query.limit,
    method: 'get'
  })
}
export function GetAllSquareUserAdmin() {
  return request({
    url: 'square/GetAllSquareUserAdmin',
    method: 'get'
  })
}
export function audit(data) {
  return request({
    url: 'square/audit/',
    method: 'post',
    data: data
  })
}
export function updateUser(userId, type) {
  return request({
    url: 'square/updateUser/'+userId+'/'+type,
    method: 'post',
  })
}
