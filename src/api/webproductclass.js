import request from '@/utils/request'

export function getProductClasslist() {
  return request({
    url: 'WebProductClass/getProductClasslist',
    method: 'get'
  })
}
export function getProductClassNameByid(classid) {
  return request({
    url: 'WebProductClass/getProductClassNameByid/' + classid,
    method: 'get'
  })
}