import request from '@/utils/request'

export function boxCard() {
  return request({
    url: 'statistic/boxCard',
    method: 'get'
  })
}

export function panelGroup() {
  return request({
    url: 'statistic/panelGroup',
    method: 'get'
  })
}