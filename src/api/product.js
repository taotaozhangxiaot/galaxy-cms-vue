import request from '@/utils/request'

export function createProduct(data) {
  return request({
    url: '/product/create',
    method: 'post',
    data: data,
    dataType: 'json',
    crossDomain: true,
    processData: false,
    contentType: false,
  })
}

export function getAllProduct(query) {
  return request({
    url: 'product/getAllProduct/' + query.page + '/' + query.limit,
    method: 'get'
  })
}

export function DelectProductById(id) {
  return request({
    url: 'product/DelectProductById/' + id,
    method: 'get'
  })
}

export function getProductById(id) {
  return request({
    url: 'product/getProductById/' + id,
    method: 'get'
  })
}

export function audit(data) {
  return request({
    url: 'product/audit',
    method: 'post',
    data: data
  })
}
export function newProductClass(data) {
  return request({
    url: '/ProductClass/newProductClass',
    method: 'post',
    data: data,
    dataType: 'json',
    crossDomain: true,
    processData: false,
    contentType: false,
  })
}

export function allProductClass(query) {
  return request({
    url: 'ProductClass/allProductClass/' + query.page + '/' + query.limit,
    method: 'post'
  })
}

export function DeleteProductClass(id) {
  return request({
    url: 'ProductClass/DeleteProductClass/' + id,
    method: 'get'
  })
}

export function getClassNameById(id) {
  return request({
    url: 'ProductClass/getClassNameById/'+ id,
    method: 'get',
  })
}

export function getAllClassName(data) {
  return request({
    url: 'ProductClass/getAllClassName',
    method: 'get',
    data
  })
}