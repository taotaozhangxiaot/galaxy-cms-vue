// axios 发送ajax请求
import request from "@/utils/request";

//查询订单列表

export function list() {
  return request({
    url: "/pay/order/list",
    method: "get",
  });
}

export function paylistById(userid) {
  return request({
    url: "/pay/order/list/ByUser/" + userid,
    method: "get",
  });
}
export function orderSendMessage(data) {
  return request({
    url: "/pay/order/orderSendMessage",
    method: "post",
    data: data,
  });
}
export function orderMessageList(orderId) {
  return request({
    url: "/pay/order/orderMessageList/"+orderId,
    method: "get",
  });
}

export function payOrder(data) {
  return request({
    url: "/pay/pay",
    method: "post",
    data: data,
  });
}
