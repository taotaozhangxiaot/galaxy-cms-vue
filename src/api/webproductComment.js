import request from '@/utils/request'

export function getallProductComment(id) {
  return request({
    url: 'WebProductComment/getallProductComment/' + id,
    method: 'get',
  })
}

export function addProductComment(data) {
  return request({
    url: 'WebProductComment/addProductComment',
    method: 'post',
    data: data,
    dataType: 'json',
    crossDomain: true,
    processData: false,
    contentType: false,
  })
}

export function getProductCommentnum(id) {
  return request({
    url: 'WebProductComment/getProductCommentnum/' + id,
    method: 'get',
  })
}

export function getNewProductComment(num) {
  return request({
    url: 'WebProductComment/getNewProductComment/' + num,
    method: 'get',
  })
}