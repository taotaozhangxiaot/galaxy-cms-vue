import request from '@/utils/request'

export function getList(query) {
  return request({
    url: 'log/page/' + query.page + '/' + query.limit,
    method: 'get'
  })
}