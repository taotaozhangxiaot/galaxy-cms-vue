import request from '@/utils/request'

export function getAllProduct(query) {
  return request({
    url: 'WebProduct/getAllProduct/' + query.page + '/' + query.limit,
    method: 'get'
  })
}

export function getProductFilter(query,filter) {
  return request({
    url: 'WebProduct/getProductFilter/' + query.page + '/' + query.limit + '/' + query.class + '/' + filter,
    method: 'get'
  })
}

export function getAllProductNumber() {
  return request({
    url: 'WebProduct/getAllProductNumber',
    method: 'get'
  })
}

export function getProductById(id) {
  return request({
    url: 'WebProduct/getProductById/'+id,
    method: 'get'
  })
}

export function getNewProduct(num,filter) {
  return request({
    url: 'WebProduct/getNewProduct/' + num + '/' + filter,
    method: 'get'
  })
}

export function FindproductByNum(title,num) {
  return request({
    url: 'WebProduct/findproductbynum/'+ title +"/" + num,
    method: 'get'
  })
}

export function loveproduct(id) {
  return request({
    url: '/WebProduct/product/'+ id +'/love',
    method: 'get',
  })
}

export function getProductByClass(id) {
  return request({
    url: '/WebProduct/getProductByClassId/'+ id ,
    method: 'get',
  })
}

export function FindAllProduct(query) {
  return request({
    url: 'WebProduct/FindAllProduct/'+query.content + '/' + query.page + '/' + query.limit,
    method: 'get'
  })
}

export function getPrenewsProduct(id) {
  return request({
    url: '/WebProduct/getPrenewsProduct/' + id,
    method: 'get',
  })
}

export function getLastnewsProduct(id) {
  return request({
    url: '/WebProduct/getLastnewsProduct/' + id,
    method: 'get',
  })
}

export function viewproduct(id) {
  return request({
    url: '/WebProduct/product/'+ id +'/view',
    method: 'get',
  })
}